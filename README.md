# A simple Node.js based chain translator

## Usage
 
 - install Node.js 8.6+
 - run `npm install` in the project folder
 - modify the constants `langOrder` and `input` in the `index.js` accordingly
 - run `npm start`
