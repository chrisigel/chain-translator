var translate = require('node-google-translate-skidz');

const shuffleArray = arr =>
  arr
    .map(a => [Math.random(), a])
    .sort((a, b) => a[0] - b[0])
    .map(a => a[1]);

const langs = [
  'lt',
  'ru',
  'el',
  'pt',
  'af',
  'ro',
  'en',
  'bn',
  'ml',
  'yi',
  'fi',
  'fr',
  'ko',
  'nl',
  'is',
  'sv',
  'da',
  'es',
];

const randomLangs = ['de', 'en', ...shuffleArray(langs).slice(0, 7), 'en', 'de'];

const input = `
Dr. Alban  mischte 45 verschiedene Cocktails, hiernach zerhackte er zusätzlich seine eigenen Fingerknöchel, wodurch eine Art von geistig Zerreißen in seiner Methküche beschworen wurde. Im vergangenen Jahr überstieg die Zahl der althergebrachten Satansbeschwörungen erstmals die Ausbeute kapverdischer Menschenfresser - aus toilettenförmigen Skulpturen wurden viskose Schleime extrahiert, um als Vorstufe des Wassermannzeitalters rituell genutzt zu werden. Der Hohepriester Ottmar Hitzfeld überwarf sich zeitgleich mit frisch abgezogenen Paninistickern, brach die Klapptreppe seines Wohnwagens um, um endlich einmal ungestört und ohne Störung zeitlich befreit einen Hurensohn zu zeugen. Währenddessen knüllte Dr. Bienenkönig sein Manuskript in Martin Pöhners Nasenloch. Der Automat am Hauptbahnhof evolvierte zu einem bedrohlichen, schlauchförmigen und siebzehn Meter breiten, nachtschwarzen Tausendfüßler, der circa 7437 Kilo Wechselgeld pro Minute auf umstehende Passanten gebar. Dieses Schauspiel vergrämte einige Tausendfüßlergegner, die, bewaffnet mit Facebookposts sowie Schraubenziehern, sich sogleich daran machten, das Ungetüm hektisch zu pieksen. Die Reaktion des Biests war vorhersehbar - es konjugierte galvanisierend alle nahen Brückenpfeiler und stürmte Gallerte speiend die Bahnhofshalle, ein Teil der Glasfront verschmolz durch die extreme Hitze des Auswurfs zu nadelförmigen Quadern. Alarmierte Polizisten unterwarfen sich klugerweise sofort ihrer Gottheit, die allerdings wenig Barmherzigkeit zeigte und die Häute der Ordnungshüter mit außerweltlichem Hass verdampfte. Sogleich wurde der Notstand international abgeschafft. Im benachbarten Japan entstand parallel eine Religion, welche den Tausendfüßler als Schöpfergott und Ausgeburt Satans verehrte. Die Schäden waren vernachlässigbar, mit Ausnahme der Quader aus geschmolzenem Glas, die lästige Dellen an vorbeifliegenden Flugtaxis verursachten. 
Das Wechselgeld sammelten ausgewählte Zentralbanken ein, um damit ein Abbildnis eines jüdischen Tausendfüßlers zu errichten, welches anschließend von palästinensischen Aufständischen beschmutzt zu werden drohte, aller Abschaum der Welt wurde seinerzeit exmatrikuliert und chemisch verzinkt.
`;

let progress = 0;

function t(from, to, text) {
  return new Promise(resolve => {
    translate(
      {
        source: from,
        target: to,
        text,
      },
      result => {
        console.log(
          `${randomLangs[progress].toUpperCase()} -> ${randomLangs[progress + 1].toUpperCase()} (${Math.round(
            ((progress + 1) / randomLangs.length) * 100
          )} %)`
        );
        progress++;
        resolve(result.translation);
      }
    );
  });
}

async function getPromise(i = 0) {
  const from = randomLangs[randomLangs.length - 2 - i];
  const to = randomLangs[randomLangs.length - 1 - i];

  return t(from, to, i < randomLangs.length - 2 ? await getPromise(++i) : input);
}

(async () => {
  const result = await getPromise();
  console.log();
  console.log('------------------------------ INPUT ------------------------------');
  console.log(input);
  console.log();
  console.log('------------------------------ OUTPUT ------------------------------');
  console.log(result);
})();
